<?php

namespace Drupal\racing\Installer\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the site configuration form.
 */
class MenuConfigureForm extends ConfigFormBase {

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'racing_menu_configure_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#title'] = $this->t('Configure menu items');

    // Register menu link and URL.
    $form['register'] = array(
      '#type' => 'checkbox',
      '#title' => t('Registration page'),
    );
    $form['register_url'] = array(
      '#type' => 'textfield',
      '#title' => t('Registration URL'),
      '#states' => array(
        'visible' => array(
          ':input[name="register"]' => array('checked' => TRUE),
        ),
      ),
    );

    // Basic pages (menu items) to include.
    $form['course_details'] = array(
      '#type' => 'checkbox',
      '#title' => t('Include a course details page'),
    );
    $form['aid_stations'] = array(
      '#type' => 'checkbox',
      '#title' => t('Include an aid stations page'),
    );
    $form['parking'] = array(
      '#type' => 'checkbox',
      '#title' => t('Include a parking info page'),
    );
    $form['rules'] = array(
      '#type' => 'checkbox',
      '#title' => t('Include a rules page'),
    );
    $form['directions'] = array(
      '#type' => 'checkbox',
      '#title' => t('Include a directions page'),
    );
    $form['volunteers'] = array(
      '#type' => 'checkbox',
      '#title' => t('Include a volunteers page'),
    );
    $form['sponsors'] = array(
      '#type' => 'checkbox',
      '#title' => t('Include a sponsors page'),
    );
    $form['news'] = array(
      '#type' => 'checkbox',
      '#title' => t('Include a news page'),
    );

    $form['actions'] = array('#type' => 'actions');
    $form['actions']['save'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save and continue'),
      '#button_type' => 'primary',
      '#submit' => array('::submitForm'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $buildInfo = $form_state->getBuildInfo();
    $install_state = $buildInfo['args'];
    $install_state[0]['form_state_values'] = $form_state->getValues();
    $buildInfo['args'] = $install_state;
    $form_state->setBuildInfo($buildInfo);
  }

}
