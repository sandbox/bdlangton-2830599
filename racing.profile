<?php

/**
 * @file
 * Enables modules and site configuration for the racing site installation.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\node\Entity\Node;

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function racing_form_install_configure_form_alter(&$form, FormStateInterface $form_state) {
  // Add a value as example that one can choose an arbitrary site name.
  $form['site_information']['site_name']['#placeholder'] = t('My Race Website');
}

/**
 * Implements hook_install_tasks().
 */
function racing_install_tasks(&$install_state) {

  $tasks = array(
    'racing_menu_configure_form' => array(
      'display_name' => t('Configure menu items'),
      'type' => 'form',
      'function' => 'Drupal\racing\Installer\Form\MenuConfigureForm',
    ),
    'racing_menu_install' => array(
      'display_name' => t('Install additional menu items'),
      'type' => 'normal',
    ),
  );

  return $tasks;
}

/**
 * Installs the racing menu items.
 *
 * @param array $install_state
 *   The install state.
 */
function racing_menu_install(&$install_state) {
  // Form state values array.
  $values = !empty($install_state['form_state_values']) ? $install_state['form_state_values'] : array();

  // Register menu link.
  if (!empty($values['register'])) {
    $menu_link = MenuLinkContent::create([
      'title' => 'Register',
      'link' => ['uri' => $values['register_url']],
      'menu_name' => 'main',
      'expanded' => TRUE,
    ]);
    $menu_link->save();
  }

  // The following results create a basic page and then a menu link to them.
  $pages = array(
    'course_details' => 'Course Details',
    'aid_stations' => 'Aid Stations',
    'parking' => 'Parking',
    'rules' => 'Rules',
    'directions' => 'Directions',
    'volunteers' => 'Volunteers',
    'sponsors' => 'Sponsors',
    'news' => 'News',
  );
  foreach ($pages as $key => $page) {
    if (!empty($values[$key])) {
      $node = Node::create([
        'type' => 'page',
        'title' => $page,
        'uid' => 1,
      ]);
      $node->save();
      $menu_link = MenuLinkContent::create([
        'title' => $page,
        'link' => ['uri' => 'internal:/node/' . $node->id()],
        'menu_name' => 'main',
        'expanded' => TRUE,
      ]);
      $menu_link->save();
    }
  }
}
